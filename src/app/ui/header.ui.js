import { LitElement, html } from "lit";

export class HeaderUI extends LitElement {
  static get properties() {
    return {
      navMenu: { type: Boolean },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.navMenu = false;
  }

  render() {
    return html`
      <a class="header__title" href="/">
        <h1>Sun<span class="header__title--light">lab</span></h1>
      </a>
      <div
        class="header__icon fa-solid fa-bars"
        @click="${this.handleClickNav}"
      ></div>
      <nav class=${`header__nav ${!this.navMenu ? "hideShow" : ""}`}>
        <a id="navHome" href="/" @click="${this.handleClickLink}">Home</a>
        <a id="navSunlab" href="/sunlab" @click="${this.handleClickLink}"
          >Sunlab</a
        >
      </nav>
    `;
  }

  // Internal methods
  // Show/hide navigation menu when clicking on the icon
  handleClickNav() {
    this.navMenu = !this.navMenu;
  }
  // Hide menu after clicking on a link
  handleClickLink() {
    this.navMenu = false;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("header-ui", HeaderUI);

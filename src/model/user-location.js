export class UserLocation {
  constructor({ city, country, latitude, longitude }) {
    this.city = city;
    this.country = country;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}

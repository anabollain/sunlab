import { LitElement, html } from "lit";
// Models
import { UserData } from "../../model/user-data";
// Usecases
import { RequestSunDataUseCase } from "../../usecases/request-sundata.usecase";
import { FormValidationUseCase } from "../../usecases/form-validation.usecase";

export class DataForm extends LitElement {
  static get properties() {
    return {
      cityInput: { type: String },
      countryInput: { type: String },
      dateInput: { type: String },
      userData: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.cityInput = "";
    this.countryInput = "";
    this.dateInput = "";
    this.userData = new UserData({
      city: "",
      country: "",
      date: "",
    });
  }

  render() {
    return html`
      <form class="form" name="dataForm" @submit="${this.preventSubmit}">
        <section class="form__section">
          <fieldset class="form__set">
            <p class="form__field">
              <label for="city">City</label>
              <input
                type="text"
                id="city"
                name="city"
                placeholder="Enter a city"
                @change="${this.setInputValues}"
                .value="${this.cityInput}"
              />
              <small></small>
            </p>
            <p class="form__field">
              <label for="country">Country</label>
              <input
                type="text"
                id="country"
                name="country"
                placeholder="Enter a country"
                @change="${this.setInputValues}"
                .value="${this.countryInput}"
              />
              <small></small>
            </p>
          </fieldset>
          <fieldset class="form__set">
            <p class="form__field">
              <label for="date">Date</label>
              <input
                type="date"
                id="date"
                name="date"
                max="9999-12-31"
                @change="${this.setInputValues}"
                .value="${this.dateInput}"
              />
              <small class="optional"
                >Optional field, if you leave it empty it will return today's
                data.</small
              >
            </p>
          </fieldset>
        </section>
        <footer class="form__footer">
          <button
            class="form__footer--btn submit"
            type="submit"
            @click="${this.submitForm}"
          >
            Go
          </button>
          <button
            class="form__footer--btn reset"
            type="reset"
            @click="${this.resetForm}"
          >
            Reset
          </button>
        </footer>
      </form>
    `;
  }

  // Internal methods
  // Get input values from user
  setInputValues(e) {
    // For binding input values and properties
    this[e.target.name + "Input"] = e.target.value;
    // For usecase execution
    this.userData[e.target.name] = e.target.value;
  }

  // Prevent refresh
  preventSubmit(e) {
    e.preventDefault();
  }

  // Reset form
  resetForm() {
    this.cityInput = "";
    this.countryInput = "";
    this.dateInput = "";

    this.userData = {
      city: "",
      country: "",
      date: "",
    };

    this.setValidationMsg("city", "");
    this.setValidationMsg("country", "");
  }

  // Form validation
  setValidationMsg(id, message) {
    const field = document.getElementById(id);
    const inputControl = field.parentElement;
    const errorDisplay = inputControl.querySelector("small");

    errorDisplay.innerText = message;
    if (message !== "") {
      errorDisplay.classList.add("error");
    } else {
      errorDisplay.classList.remove("error");
    }
  }

  validateForm() {
    const isValid = FormValidationUseCase.execute(this.userData);

    this.setValidationMsg("city", isValid.msg.city);
    this.setValidationMsg("country", isValid.msg.country);
    if (isValid.success === true) {
      this.userData.date = isValid.date;
    }

    return isValid.success;
  }

  // Methods that transfer information by events to parent component
  // Submit form
  async submitForm() {
    // Validate form
    if (!this.validateForm()) return;

    // Execute use case to obtain sun data
    const result = await RequestSunDataUseCase.execute(this.userData);
    // Send obtained data from API and user input data through a custom event
    const newResult = new CustomEvent("new:result", {
      bubbles: true,
      detail: {
        userData: { ...this.userData },
        result: { ...result },
      },
    });
    this.dispatchEvent(newResult);

    // Reset inputs
    this.resetForm();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("data-form", DataForm);

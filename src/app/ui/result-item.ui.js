import { LitElement, html } from "lit";

export class ResultItemUI extends LitElement {
  static get properties() {
    return {
      sunData: { type: Object },
      locationData: { type: Object },
      userData: { type: Object },
    };
  }

  render() {
    const setClassByDaylength =
      Math.floor(parseInt((this.sunData.dayLength).slice(0, -3))) > 12
        ? "--light"
        : "--dark";

    return html`
      <article class=${`item theme${setClassByDaylength}`}>
        <h3 class="item__title">
          ${this.locationData?.city}, ${this.locationData?.country}
        </h3>
        <ul class="item__list">
          <li role="listitem">
            Sunrise: <span>${this.sunData?.sunrise}</span>
          </li>
          <li role="listitem">Sunset: <span>${this.sunData?.sunset}</span></li>
          <li role="listitem">
            Solar noon: <span>${this.sunData?.solarNoon}</span>
          </li>
          <li role="listitem">
            Day length: <span>${this.sunData?.dayLength}</span>
          </li>
          <li role="listitem">Date: <span>${this.userData?.date}</span></li>
        </ul>
        <button class="item__btn" @click="${this.clickDeleteResult}">
          <span class="fa-solid fa-circle-minus"></span>
        </button>
      </article>
    `;
  }

  // Methods that transfer information by events to parent component
  // Delete the selected element with the specific id value
  clickDeleteResult() {
    const deleteResult = new CustomEvent("delete:result", {
      bubbles: true,
      detail: {
        resultId: this.sunData.id,
      },
    });
    this.dispatchEvent(deleteResult);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("result-item-ui", ResultItemUI);

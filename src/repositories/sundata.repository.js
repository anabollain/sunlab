import axios from "axios";

export class SunDataRepository {
  async getSunData(userDataModel, userLocationModel) {
    return await (
      await axios.get(
        `https://api.sunrise-sunset.org/json?lat=${userLocationModel.latitude}&lng=${userLocationModel.longitude}&date=${userDataModel.date}`
      )
    ).data.results;
  }
}

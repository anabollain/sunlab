export class SunData {
  constructor({ id, sunrise, sunset, solarNoon, dayLength }) {
    this.id = id;
    this.sunrise = sunrise;
    this.sunset = sunset;
    this.solarNoon = solarNoon;
    this.dayLength = dayLength;
  }
}

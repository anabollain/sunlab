import { LitElement, html } from "lit";

export class LoadingUI extends LitElement {
  render() {
    return html`
      <div class="loading__ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("loading-ui", LoadingUI);

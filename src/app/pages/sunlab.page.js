// Components
import "../components/result-detail.component";
import "../components/data-form.component";
import "../components/result-list.component";


export class SunLab extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = /*html*/ `
        <result-detail id="resultDetail" class="result"></result-detail>
        <data-form id="dataForm"></data-form>
        <result-list id="resultList"></result-list>
    `;

    // Data form internal events
    this.addEventListener("new:result", (e) => {
      this.querySelector("#resultDetail")._setNewResult(e.detail);
    });

    // Result detail internal events
    this.addEventListener("add:result", (e) => {
      this.querySelector("#resultList")._addResult(e.detail);
    });
  }
}

customElements.define("sunlab-page", SunLab);

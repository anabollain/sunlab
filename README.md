# ☀️ SUNLAB. SPA

## Introduction

Hey there! ✨

This project is a **Vanilla SPA** (Single Page Application) developed with non-reusable Web Components and Webpack as a module bundler.

It uses **Jest** testing framework for automated unit testing and **Cypress** for E2E testing involving the entire application.

Some automation tools have also been implemented, such as **Husky** to create Git Hooks to ensure all commits comply with Conventional Commit standard and all tests run before each commit.

The deployment of the project code is done through a **GitLab CI** (Continuous Integration) pipeline, with several stages that are triggered in each push to the remote Gitlab repository:

- _Build stage_: it creates the project static files for the hosting stage.
- _Deploy_: it uses Docker in Docker to create the image that will be pushed to Docker Hub into the project repository, you can check it [here](https://hub.docker.com/repository/docker/anabollain/sunlab-app/general).
- _Sonarcloud check_: a cloud-based code analysis service that provides continuous inspection of the codebase to detect potential issues and vulnerabilities.
- _Hosting_: the app is hosted in Firebase web hosting platform provided by Google, you can visit it [here](https://sunlab-app.web.app/).

---

## Content

1. [Project Structure](#project-structure)
2. [Project Description](#project-description)
3. [Process](#process)
   1. [Setup the Environment](#setup-the-environment)
   2. [Web Components](#web-components)
   3. [Testing](#testing)
4. [How to Run the App](#how-to-run-the-app)
   1. [Instructions](#instructions)
   2. [Docker](#docker)
5. [Useful Resources](#useful-resources)
6. [Author](#author)

---

<a name='project-structure'/>

## Project structure

```
|– .husky
|– cypress
|   |– e2e
|– nginx
|– src
|   |– app
|     |– components
|     |– pages
|     |– ui
|   |– images
|   |– model
|   |– repositories
|   |– styles
|     |– components
|     |– core
|     |– pages
|     |– ui
|     |– main.css
|   |– usecases
|   |– index.html
|   |– main.js
|– test
```

---

<a name='project-description'/>

## Project Description

This web application allows the user to consult data regarding the sunset, sunrise, solar noon and day length according to a specified location, as well as a specific date if wanted.

**Several API calls**

The data is obtained by several API requests. The first API by [Geonames](https://www.geonames.org/) returns the location with the latitude and longitude parameters corresponding to the city and country specified by the user.

Once this information is obtained, two parallel API calls are executed to improve the app performance: one to obtain the data regarding the sun in UTC format from the API [Sunrise-Sunset](https://sunrise-sunset.org/api?ref=apislist.com), and the other to obtain the time zone to be able to convert UTC time into the local time corresponding to the introduced city and country with the API [Geonames](https://www.geonames.org/).

**Main workflow**

The user first accesses the home page, with a visual animation and a small description that will explain the main functionality of the app. By hovering over the numbers, she or he will obtain information about the different steps.

By clicking on the start button, a form will appear with the following inputs:

- City: required, if missing, it will show an error message below.
- Country: required, if missing, it will show an error message below.
- Date: optional, with a specific format to be respected if filled.

Once all required inputs are filled, the user will obtain the result by clicking on the Go button.

**Result**

If the introduced city and country are available on the API, the result will be shown above the form with an add and delete button. This way, the user can save it on a local list that will appear beneath the form or delete it in order to start a new search.

If no results are found, a customized message will appear instead.

**Local List**

The aim of the list is to allow the user to compare different cities all over the world, through different dates if wanted. Depending on the day length of the result, the background color will either be pink (more than 12 hours) or black.

**Extra functionalities**

Some extra functionalities have been implemented to improve the user's experience, such as:

- All searches and results added to the list are saved in the user's local storage, so when refreshing the page they will still be available.
- If the user wants to restart a new search from scratch, she or he will only have to click the reset button.
- If the introduced URL does not exist, the user will be redirected either to the home page or the sunlab page.
- Responsive design.

---

<a name='process'/>

## Process

<a name='setup-the-environment'/>

### 💫 Setup the Environment

This project uses [Webpack](https://webpack.js.org/), a free and open-source module bundler for Javascript, to transform front-end assets such as HTML, CSS and JS into readable and executable JS files by the client or browser.

In other words, it bundles the code and outputs a transpiled version down to the target as specified in the configuration files. In this case, the project consists on three different configuration files:

- _Common_: to set the common rules for loaders handling CSS, HTML and Javascript files, and a plugin for generating HTML files with injected Javascript code.
- _Development_: with additional settings for handling client-side routing and source map generation for debugging purposes.
- _Production_: with additional settings for extracting CSS into separate files and generating source maps for debugging purposes in production.

The project also includes `JS Semi-Standard`, a linter for JavaScript that follows a set of style rules, such as using semicolons to terminate statements or enforcing strict equality checks.

Concerning Git workflow, the project uses [Husky](https://typicode.github.io/husky/#/) for adding Git hooks that run automatically when a commit is made. It has two configuration files, a `pre-commit` to run all tests before making a commit, and a `commit-msg` to check that the commit messages comply with the Conventional Commit standard.

As for routing purposes, the project uses `Vaadin Router`, a Javascript routing library designed for building SPAs with web components.

<a name='web-components'/>

### 🔮 Web Components

The interface of the app has been developed with non-reusable Web Components without using the Shadow DOM due to project requirements.

By disabling the additional encapsulation provided by the Shadow DOM, the components inherit the styles and behavior of the surrounding page.

Therefore, all style sheets are generic to the app. Their division by components is merely for organizational reasons and in order to make it more readable.

```
|– src
|   |– app
|     |– components
|     |– pages
|     |– ui
|   |– styles
|     |– components
|     |– core
|     |– pages
|     |– ui
|     |– main.css
|   |– index.html
|   |– main.js
```

<a name='testing'/>

### 🧪 Testing

**Unit Testing with Jest**

For automated browser testing purposes, the project uses [Jest](https://jestjs.io/), a Javascript testing framework that in this case relies on Babel for transpiling code.

The business logic is implemented in a `usecase` that is tested through all the different steps by a unit testing.

To avoid making real requests to the different APIs each time the tests run, a mocking technique has been used. This mock simulates the behavior of the actual APIs without making real requests.

```
|– cypress
|   |– e2e
|– src
|   |– model
|   |– repositories
|   |– usecases
|– test
```

**Cypress e2e**

To perform end-to-end (E2E) testing for the entire frontend application, the project uses [Cypress](https://www.cypress.io/) framework to ensure all parts are working together correctly.

There are several tests responding to the main app components:

- Routing between the different pages that compose the app.
- Form filled by the user.
- Result printed once the request has been made.
- Local result list where the user is able to save a specific result.

---

<a name='how-to-run-the-app'/>

## 🚀 How to run the App

<a name='instructions'/>

### Instructions

You need to have [Node](https://nodejs.org/es/) previously installed on your computer.

1. To start using this project, clone this repository into a new directory:

```console
$ git clone https://gitlab.com/anabollain/sunlab.git
```

2. Once you are on the root directory, open the shell command and install all necessary dependencies:

```console
$ npm install
```

3. Once all dependencies have been installed, you will be able to run the project typing in the shell command:

```console

$ npm start
```

4. The project will be available on the following port in your browser:

```console
http://localhost:8080
```

5. To run the tests implemented with Jest, you need to introduce the following commands on your terminal:

```console
$ npm run test
```

6. To run e2e local tests with Cypress, you must type the following command while the project is running:

```console
$ npm run cy:test
```

---

<a name='docker'/>

### 🐳 Docker

- You can initialize the project in Docker typing on the shell command:

```console
$ docker-compose up -d
```

- The project will be available on following port in your browser:

```console
http://localhost:8080
```

- You can also run a container by pulling the available image from Docker Hub typing the following:

```console
$ docker run -d -p "3000:80" anabollain/sunlab-app:0.0.0
```

---

<a name='useful-resources'/>

### Useful resources

I have faced many challenges along the app building process. I list here some useful resources that have helped me along the way, hopefully they will help you too:

- Cypress configuration issues in Windows 10 with WSL 2:

  - Step-by-step guide to install Cypress: [here](https://gist.github.com/pjobson/6b9fb926c59f58aa73d4efa10fe13654)
  - More useful information complementing the previous link: [here](https://shouv.medium.com/how-to-run-cypress-on-wsl2-989b83795fb6)

- Searching for an API rest, I leave here below some interesting links in case you are looking for one:

  - [ApisList](https://apislist.com/)
  - [RapidAPI](https://rapidapi.com/collection/list-of-free-apis)
  - [PublicApis](https://github.com/public-apis/public-apis)

---

## Link to the App

Should you like to take a look to the deployed app, [here you have the link](https://sunlab-app.web.app/).

Hope you enjoy the project as much as I did 😉

**Thank you!**

---

<a name='author'/>

## 👋 Author

This App has been developed by [**Ana Bollain**](https://github.com/anabollain).

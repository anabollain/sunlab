// Models
import { UserData } from "../src/model/user-data";
// Usecases
import { FormValidationUseCase } from "../src/usecases/form-validation.usecase";

describe("Form validation", () => {
  it("should return false with a customized message if city input is empty", () => {
    const userDataModel = new UserData({
      city: "",
      country: "Spain",
      date: "1990-09-27",
    });

    const result = FormValidationUseCase.execute(userDataModel);

    expect(result.msg.city).toBe("Please provide a city name");
    expect(result.msg.country).toBe("");
    expect(result.date).toBe("");
    expect(result.success).toBe(false);
  });

  it("should return false with a customized message if country input is empty", () => {
    const userDataModel = new UserData({
      city: "Bilbao",
      country: "",
      date: "1990-09-27",
    });

    const result = FormValidationUseCase.execute(userDataModel);

    expect(result.msg.city).toBe("");
    expect(result.msg.country).toBe("Please provide a country name");
    expect(result.date).toBe("");
    expect(result.success).toBe(false);
  });

  it("should return true with current date if city and country inputs are filled", () => {
    const userDataModel = new UserData({
      city: "Bilbao",
      country: "Spain",
      date: "",
    });

    const currentDate = new Date().toISOString().split("T")[0];

    const result = FormValidationUseCase.execute(userDataModel);

    expect(result.msg.city).toBe("");
    expect(result.msg.country).toBe("");
    expect(result.date).toBe(currentDate);
    expect(result.success).toBe(true);
  });

  it("should return true if all inputs are filled with specified date", () => {
    const userDataModel = new UserData({
      city: "Bilbao",
      country: "Spain",
      date: "1990-09-27",
    });

    const result = FormValidationUseCase.execute(userDataModel);

    expect(result.msg.city).toBe("");
    expect(result.msg.country).toBe("");
    expect(result.date).toBe(userDataModel.date);
    expect(result.success).toBe(true);
  });
});

export class FormValidationUseCase {
  static execute(userDataModel) {
    let result = {
      msg: {
        city: "",
        country: "",
      },
      date: "",
      success: false,
    };

    if (userDataModel.city === "") {
      result.msg.city = "Please provide a city name";
    } else if (userDataModel.city !== "" && userDataModel.country === "") {
      result.msg.country = "Please provide a country name";
    } else if (
      userDataModel.city !== "" &&
      userDataModel.country !== "" &&
      userDataModel.date === ""
    ) {
      result.date = new Date().toISOString().split("T")[0];
      result.success = true;
    } else {
      result.date = userDataModel.date;
      result.success = true;
    }

    return result;
  }
}

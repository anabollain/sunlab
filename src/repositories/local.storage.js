export class LocalStorage {
  static get(key, defaultValue) {
    const localStorageData = localStorage.getItem(key);
    if (localStorageData === null) {
      return defaultValue;
    } else {
      return JSON.parse(localStorageData);
    }
  }

  static set(key, value) {
    const localStorageData = JSON.stringify(value);
    localStorage.setItem(key, localStorageData);
  }

  static remove(key) {
    localStorage.removeItem(key);
  }

  static clear() {
    localStorage.clear();
  }
}

import axios from "axios";

export class LocationRepository {
  constructor() {
    this.username = "sunlab";
  }

  async getLocation(userDataModel) {
    const encodedURL = encodeURIComponent(
      `${userDataModel.city},${userDataModel.country}`
    );
    return await (
      await (
        await axios.get(
          `https://secure.geonames.org/searchJSON?q=${encodedURL}&username=${this.username}`
        )
      ).data
    ).geonames[0];
  }

  async getTimeZone(locationModel) {
    return await (
      await (
        await axios.get(
          `https://secure.geonames.org/timezoneJSON?lat=${locationModel.latitude}&lng=${locationModel.longitude}&username=${this.username}`
        )
      ).data
    ).timezoneId;
  }
}

// Main styles
import "./styles/core/reset.css";
import "./styles/core/variables.css";
import "./styles/main.css";
import "./styles/ui/header.ui.css";
// Home page styles
import "./styles/pages/home.page.css";
import "./styles/ui/logo.ui.css";
// Sunlab page styles
import "./styles/pages/sunlab.page.css";
import "./styles/components/result-detail.component.css";
import "./styles/components/data-form.component.css";
import "./styles/components/result-list.component.css";
import "./styles/ui/loading.ui.css";
import "./styles/ui/result-logo.ui.css";
import "./styles/ui/result.ui.css";

// Components
import "./app/ui/header.ui";

const header = document.getElementById("header");
header.innerHTML = `<header-ui></header-ui>`;

// Routes
import "./app/pages/home.page";
import "./app/pages/sunlab.page";

import { Router } from "@vaadin/router";

const outlet = document.getElementById("outlet");
const router = new Router(outlet);
router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/sunlab", component: "sunlab-page" },
  { path: "(/sunlab/.*)", redirect: "/sunlab" },
  { path: "(.*)", redirect: "/" },
]);

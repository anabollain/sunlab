import { LitElement, html } from "lit";
// Components
import "../ui/loading.ui";
import "../ui/result-logo.ui";
// Local Storage
import { LocalStorage } from "../../repositories/local.storage";

export class ResultDetail extends LitElement {
  static get properties() {
    return {
      result: { type: Object },
      userData: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.result = LocalStorage.get("result", null);
    this.userData = null;
  }

  render() {
    // Default state with empty inputs and no API result
    if (!this.result && !this.userData) {
      return html` <result-logo-ui></result-logo-ui> `;
    } else if (!this.result && this.userData) {
      // In case API takes a while to return information
      return html` <loading-ui></loading-ui> `;
    } else if (this.result.success === false) {
      // API is unable to return information for the specified inputs
      return html`
        <section class="result__error">
          <result-logo-ui></result-logo-ui>
          <p>Sorry, I was unable to find your data...</p>
        </section>
      `;
    } else if (this.result.sunDataModel) {
      // API returns information for the specified inputs
      return html` <ul class="result__list">
          <li class="result__list--item">
            <span class="fa-solid fa-sun"></span>
            <p>Sunrise <span>${this.result.sunDataModel.sunrise}</span></p>
          </li>
          <li class="result__list--item">
            <span class="fa-solid fa-moon"></span>
            <p>Sunset <span>${this.result.sunDataModel.sunset}</span></p>
          </li>
          <li class="result__list--item">
            <span class="fa-solid fa-up-long"></span>
            <p>
              Solar noon
              <span>${this.result.sunDataModel.solarNoon}</span>
            </p>
          </li>
          <li class="result__list--item">
            <span class="fa-solid fa-clock"></span>
            <p>
              Day length
              <span>${this.result.sunDataModel.dayLength}</span>
            </p>
          </li>
        </ul>
        <footer class="result__footer">
          <button class="result__footer--btn" @click="${this.addResultToList}">
            <span class="fa-solid fa-circle-plus"></span>
          </button>
          <button class="result__footer--btn" @click="${this.removeResult}">
            <span class="fa-solid fa-circle-minus"></span>
          </button>
        </footer>`;
    }
  }

  // Internal methods
  // Delete obtained result from API
  removeResult() {
    this.result = null;
    this.userData = null;
    LocalStorage.remove("result");
  }

  // Methods executed by parent's event listeners
  // Show new result
  _setNewResult(data) {
    this.userData = data.userData;
    this.result = data.result;
    if (this.result.sunDataModel) LocalStorage.set("result", data.result);
  }

  // Methods that transfer information by events to parent component
  addResultToList(e) {
    // Prevent refresh
    e.preventDefault();
    // Send obtained data from API and user input data through a custom event
    const addResult = new CustomEvent("add:result", {
      bubbles: true,
      detail: {
        userData: this.userData,
        sunData: this.result.sunDataModel,
        locationData: this.result.userLocationModel,
      },
    });
    this.dispatchEvent(addResult);

    // Empty values and LS once the result has been added to the list
    this.removeResult();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("result-detail", ResultDetail);

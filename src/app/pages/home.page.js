// Components
import "../ui/logo.ui";

export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = /*html*/ `
            <article class="home__article">
              <h2 class="home__hero">Tonatiuh!</h2> 
              <div>
                  <h3 class="home__article--title">The aztec god of the sun</h3>
                  <p class="home__article--desc">He will tell you all you need to know about the sun, when will it rise? When will it set? How many hours of light are there to enjoy?</p>
                  <ol class="home__article--list">
                    <li data-order="item-1" role="listitem">1 <span>Fill the inputs</span></li>
                    <li data-order="item-2" role="listitem">2 <span>Get the data</span></li>
                    <li data-order="item-2" role="listitem">3 <span>Save it in your list</span></li>
                  </ol>
                  <a class="home__article--start" href="/sunlab">Start</a>
              </div>
              <logo-ui class="home__logo"></logo-ui>
            </article>
        `;
  }
}

customElements.define("home-page", HomePage);

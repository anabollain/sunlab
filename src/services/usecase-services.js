export class UseCaseServices {
  
  static setLocalTime(date, utcTime, timeZone) {
    // Convert UTC time to local time according to the time zone
    // Date format is YYYY-MM-DD, replacing - character to ensure cross-browser compatibility
    // Output format is HH:MM AM/PM
    const localTime = new Date(
      `${date.replace(/-/g, "/")} ${utcTime} UTC`
    ).toLocaleTimeString("en-US", { timeZone: timeZone, timeStyle: "short" });
    return localTime;
  }
}

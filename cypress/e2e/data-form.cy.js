/// <reference types="Cypress" />

describe("test all data form functionalities", () => {
  beforeEach(() => {
    // Home page
    cy.visit("/");
    // Sunlab page
    cy.get(".home__article--start").click();
    // Default result interface
    cy.get("result-logo-ui").should("be.visible");
  });

  /*INVALID INPUTS*/
  it("user gets an error on submit if city input is empty", () => {
    // Empty city input
    cy.get("#city").should("have.value", "");
    // Submit
    cy.get(".submit").click();
    // Error message
    cy.get("#city + small")
      .should("be.visible")
      .contains("Please provide a city name");
    // Default result interface
    cy.get("result-logo-ui").should("be.visible");
  });

  it("user gets an error on submit if city input is filled but country input is empty", () => {
    // Filled city input
    cy.get("#city").clear().type("Bilbao");
    // Empty country input
    cy.get("#country").should("have.value", "");
    // Submit
    cy.get(".submit").click();
    // Error message
    cy.get("#country + small").should("be.visible").contains("Please provide a country name");
    // Default result interface
    cy.get("result-logo-ui").should("be.visible");
  });

  /*VALID INPUTS + AVAILABLE API REST*/
  it("user can get data on submit if city and country inputs are correctly filled and are available on the API rest", () => {
    // Fill valid city and country inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("Spain");
    // Submit
    cy.get(".submit").click();
    // Result
    cy.get(".result__list").should("be.visible");
    // Reset inputs
    cy.get("#city").should("have.value", "");
    cy.get("#country").should("have.value", "");
  });

  it("user can get data on submit with a specific date if all fields are correctly filled and are available on the API rest", () => {
    // Fill inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("Spain");
    cy.get("#date").clear().type("1990-09-27");
    // Submit
    cy.get(".submit").click();
    // Result
    cy.get(".result__list").should("be.visible");
    // Verify results
    cy.get(".result__list > :nth-child(1) > p > span").contains("8:02 AM");
    cy.get(".result__list > :nth-child(2) > p > span").contains("8:02 PM");
    cy.get(".result__list > :nth-child(3) > p > span").contains("2:02 PM");
    cy.get(".result__list > :nth-child(4) > p > span").contains("11:59");
    // Reset inputs
    cy.get("#city").should("have.value", "");
    cy.get("#country").should("have.value", "");
    cy.get("#date").should("have.value", "");
  });

  /*VALID INPUTS + NOT AVAILABLE API REST*/
  it("user gets an error message on submit if requested city and country are not available on the API rest", () => {
    // Fill inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("France");
    // Submit
    cy.get(".submit").click();
    // Result
    cy.get(".result__error").should("be.visible");
    cy.get(".result__error > p").contains(
      "Sorry, I was unable to find your data..."
    );
    // Reset inputs
    cy.get("#city").should("have.value", "");
    cy.get("#country").should("have.value", "");
  });

  it("user gets an error message on submit if requested city and country are not available on the API rest for a specific date", () => {
    // Fill inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("France");
    cy.get("#date").clear().type("1990-09-27");
    // Submit
    cy.get(".submit").click();
    // Result
    cy.get(".result__error").should("be.visible");
    cy.get(".result__error > p").contains(
      "Sorry, I was unable to find your data..."
    );
    // Reset inputs
    cy.get("#city").should("have.value", "");
    cy.get("#country").should("have.value", "");
    cy.get("#date").should("have.value", "");
  });

  /*RESET BUTTON*/
  it("user can reset the search by clicking on the reset button", () => {
    // Fill inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("France");
    cy.get("#date").clear().type("1990-09-27");
    // Click reset
    cy.get(".reset").click();
    // Reset inputs
    cy.get("#city").should("have.value", "");
    cy.get("#country").should("have.value", "");
    cy.get("#date").should("have.value", "");
  });
});

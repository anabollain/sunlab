import { LitElement, html } from "lit";
// Components
import "../ui/result-item.ui";
import "../ui/logo.ui";
// Local Storage
import { LocalStorage } from "../../repositories/local.storage";

export class ResultList extends LitElement {
  static get properties() {
    return {
      results: { type: Array },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.results = LocalStorage.get("resultList", []);
    // Result ui internal events
    this.addEventListener("delete:result", this._deleteResult);
  }

  disconnectedCallback() {
    document.removeEventListener("delete:result", this._deleteResult);
    super.disconnectedCallback();
  }

  render() {
    return html`
      ${this.results.length !== 0
        ? html`<ul class="results__list">
            ${this.results?.map(
              (result) =>
                html`<li role="listitem">
                  <result-item-ui
                    .userData="${result.userData}"
                    .sunData="${result.sunData}"
                    .locationData="${result.locationData}"
                  ></result-item-ui>
                </li>`
            )}
          </ul>`
        : html`<p class="results__msg">
            Your saved results will appear here
          </p> `}
    `;
  }

  // Methods executed by parent's event listeners
  // Add new result to the list
  _addResult(data) {
    this.results = [data, ...this.results];
    LocalStorage.set("resultList", this.results);
  }

  // Delete a result from the list
  _deleteResult(e) {
    this.results = this.results.filter(
      (result) => result.sunData.id !== e.detail.resultId
    );
    LocalStorage.set("resultList", this.results);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("result-list", ResultList);

/// <reference types="Cypress" />

describe("test routes between app pages", () => {
  /*HOME PAGE ALL DEVICES*/
  it("home page can be opened", () => {
    // Home page
    cy.visit("/");
    cy.get("home-page").should("exist");
    cy.get("sunlab-page").should("not.exist");
  });

  it("sunlab page can be visited clicking on start button", () => {
    // Sunlab page from home
    cy.visit("/");
    cy.get(".home__article--start").click();
    cy.get("sunlab-page").should("exist");
    cy.get("home-page").should("not.exist");
  });

  it("user can return to home page", () => {
    // Home page from sunlab
    cy.visit("/sunlab");
    cy.get(".header__title").click();
    cy.get("home-page").should("exist");
    cy.get("sunlab-page").should("not.exist");
  });

  /*MOBILE AND TABLET VERSION*/
  it("sunlab page can be visited", () => {
    // Sunlab page from home
    cy.viewport("iphone-3");
    cy.visit("/");
    cy.get(".header__icon").click();
    cy.get("#navSunlab").click();
    cy.get("sunlab-page").should("exist");
    cy.get("home-page").should("not.exist");
  });

  it("user can return to home page", () => {
    // Home page from sunlab
    cy.viewport("iphone-3");
    cy.visit("/sunlab");
    cy.get(".header__icon").click();
    cy.get("#navHome").click();
    cy.get("home-page").should("exist");
    cy.get("sunlab-page").should("not.exist");
  });

  it("nav menu shows and hides by clicking on header icon", () => {
    // Sunlab page from home
    cy.viewport("iphone-3");
    cy.visit("/");
    cy.get(".header__nav").should("not.be.visible");
    cy.get(".header__icon").click();
    cy.get(".header__nav").should("be.visible");
    cy.get(".header__icon").click();
    cy.get(".header__nav").should("not.be.visible");
  });

  /*DESKTOP VERSION*/
  it("sunlab page can be visited", () => {
    // Sunlab page from home
    cy.viewport("macbook-16");
    cy.visit("/");
    cy.get(".header__icon").should("not.be.visible");
    cy.get("#navSunlab").click();
    cy.get("sunlab-page").should("exist");
    cy.get("home-page").should("not.exist");
  });

  it("user can return to home page", () => {
    // Home page from sunlab
    cy.viewport("macbook-16");
    cy.visit("/sunlab");
    cy.get("#navHome").click();
    cy.get("home-page").should("exist");
    cy.get("sunlab-page").should("not.exist");
  });
});

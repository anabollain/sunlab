export class UserData {
  constructor({ city, country, date }) {
    this.city = city;
    this.country = country;
    this.date = date;
  }
}

// Repositories
import { LocationRepository } from "../repositories/location.repository";
import { SunDataRepository } from "../repositories/sundata.repository";
// Models
import { UserLocation } from "../model/user-location";
import { SunData } from "../model/sun-data";
// Services
import { UseCaseServices } from '../services/usecase-services';
// Crypto id
import { v4 as uuidv4 } from "uuid";

export class RequestSunDataUseCase {
  // Returns sun data for the location and date specified in userDataModel
  static async execute(userDataModel) {
    // Location API
    const locationRepository = new LocationRepository();
    // Get location and coordinates from API
    const userLocationApi = await locationRepository.getLocation(userDataModel);

    // Stop process if returned data is empty
    if (!userLocationApi) {
      return {
        success: false,
      };
    }

    // Create variable with received API data
    const userLocationModel = new UserLocation({
      city: userLocationApi.name,
      country: userLocationApi.countryName,
      latitude: userLocationApi.lat,
      longitude: userLocationApi.lng,
    });

    // Sun data API
    const sunDataRepository = new SunDataRepository();

    // Get sun data and time zone with parallel calls to APIs
    const [sunDataApi, timeZone] = await Promise.all([
      sunDataRepository.getSunData(userDataModel, userLocationModel),
      locationRepository.getTimeZone(userLocationModel),
    ]);

    // Create variable with unique id and received API data transformed to local time
    const sunDataModel = new SunData({
      id: uuidv4(),
      sunrise: UseCaseServices.setLocalTime(userDataModel.date, sunDataApi.sunrise, timeZone),
      sunset: UseCaseServices.setLocalTime(userDataModel.date, sunDataApi.sunset, timeZone),
      solarNoon: UseCaseServices.setLocalTime(
        userDataModel.date,
        sunDataApi.solar_noon,
        timeZone
      ),
      dayLength: sunDataApi.day_length.slice(0, -3), // API returns HH:MM:SS, removing last 3 characters corresponding to seconds
    });

    return { userLocationModel, sunDataModel };
  }
}

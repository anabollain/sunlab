// Repositories
import { LocationRepository } from "../src/repositories/location.repository";
import { SunDataRepository } from "../src/repositories/sundata.repository";
// Models
import { UserData } from "../src/model/user-data";
// Usecases
import { RequestSunDataUseCase } from "../src/usecases/request-sundata.usecase";

jest.mock("../src/repositories/location.repository.js");
jest.mock("../src/repositories/sundata.repository.js");

describe("Request sun data", () => {
  beforeEach(() => {
    LocationRepository.mockClear();
    SunDataRepository.mockClear();
  });

  it("should get sun data with user's location and selected date", async () => {
    const userDataModel = new UserData({
      city: "Bilbao",
      country: "Spain",
      date: "1990-09-27",
    });

    LocationRepository.mockImplementation(() => {
      return {
        getLocation: () => {
          return {
            name: "Bilbao",
            countryName: "Spain",
            lat: "43.26271",
            lng: "-2.92528",
          };
        },
        getTimeZone: () => {
          return "Europe/Madrid";
        },
      };
    });

    SunDataRepository.mockImplementation(() => {
      return {
        getSunData: () => {
          return {
            sunrise: "6:02:53 AM",
            sunset: "6:02:36 PM",
            solar_noon: "12:02:45 PM",
            day_length: "11:59:43",
          };
        },
      };
    });

    const { userLocationModel, sunDataModel } =
      await RequestSunDataUseCase.execute(userDataModel);

    expect(userLocationModel.latitude).toBe("43.26271");
    expect(userLocationModel.longitude).toBe("-2.92528");

    expect(sunDataModel.sunrise).toBe("8:02 AM");
    expect(sunDataModel.sunset).toBe("8:02 PM");
    expect(sunDataModel.solarNoon).toBe("2:02 PM");
    expect(sunDataModel.dayLength).toBe("11:59");
  });

  it("should get sun data in local time", async () => {
    const userDataModel = new UserData({
      city: "New York",
      country: "US",
      date: "1990-09-27",
    });

    LocationRepository.mockImplementation(() => {
      return {
        getLocation: () => {
          return {
            name: "New York",
            countryName: "United States",
            lat: "40.71427",
            lng: "-74.00597",
          };
        },
        getTimeZone: () => {
          return "America/New_York";
        },
      };
    });

    SunDataRepository.mockImplementation(() => {
      return {
        getSunData: () => {
          return {
            sunrise: "10:47:07 AM",
            sunset: "10:46:53 PM",
            solar_noon: "4:47:00 PM",
            day_length: "11:59:46",
          };
        },
      };
    });

    const { userLocationModel, sunDataModel } =
      await RequestSunDataUseCase.execute(userDataModel);

    expect(userLocationModel.latitude).toBe("40.71427");
    expect(userLocationModel.longitude).toBe("-74.00597");

    expect(sunDataModel.sunrise).toBe("6:47 AM");
    expect(sunDataModel.sunset).toBe("6:46 PM");
    expect(sunDataModel.solarNoon).toBe("12:47 PM");
    expect(sunDataModel.dayLength).toBe("11:59");
  });

  it("should throw an error", async () => {
    const userDataModel = new UserData({
      city: "Bilbao",
      country: "Francia",
    });

    LocationRepository.mockImplementation(() => {
      return {
        getLocation: () => {
          return undefined;
        },
      };
    });

    const error = await RequestSunDataUseCase.execute(userDataModel);

    expect(error.success).toBe(false);
  });
});

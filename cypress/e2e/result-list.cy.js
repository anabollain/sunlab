/// <reference types="Cypress" />

describe("test delete functionality on result list component", () => {
  beforeEach(() => {
    // Home page
    cy.visit("/");
    // Sunlab page
    cy.get(".home__article--start").click();
    // Fill valid inputs
    cy.get("#city").clear().type("Paris");
    cy.get("#country").clear().type("Francia");
    cy.get("#date").clear().type("1990-09-27");
    // Submit
    cy.get(".submit").click();
    // Add to list
    cy.get(".result__footer > :nth-child(1) > .fa-solid").click();
    // Fill valid inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("Spain");
    cy.get("#date").clear().type("1990-09-27");
    // Submit
    cy.get(".submit").click();
    // Add to list
    cy.get(".result__footer > :nth-child(1) > .fa-solid").click();
  });

  it("user can remove the selected result from the list by clicking on delete button", () => {
    // Items should exist
    cy.get(".results__msg").should("not.exist");
    cy.get(":nth-child(1) > result-item-ui > .item").should(
      "exist"
    );
    cy.get(":nth-child(2) > result-item-ui > .item").should(
      "exist"
    );
    // Check titles
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__title"
    ).contains("Bilbao, Spain");
    cy.get(
      ":nth-child(2) > result-item-ui > .item > .item__title"
    ).contains("Paris, France");
    // Item can be removed from the list by clicking delete button
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__btn"
    ).click();
    // Only one result left
    cy.get(":nth-child(1) > result-item-ui > .item").should(
      "exist"
    );
    cy.get("results__list:nth-child(2) > result-item-ui > .item").should(
      "not.exist"
    );
    // Check content of remaining result
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__title"
    ).contains("Paris, France");
    // Default result interface
    cy.get("result-logo-ui").should("exist");
  });
});

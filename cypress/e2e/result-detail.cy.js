/// <reference types="Cypress" />

describe("test add and delete functionalities on result detail component", () => {
  beforeEach(() => {
    // Home page
    cy.visit("/");
    // Sunlab page
    cy.get(".home__article--start").click();
    // Fill valid inputs
    cy.get("#city").clear().type("Bilbao");
    cy.get("#country").clear().type("Spain");
    cy.get("#date").clear().type("1990-09-27");
    // Submit
    cy.get(".submit").click();
  });

  it("user can add the result to the list by clicking on add button", () => {
    // Click add
    cy.get(".result__footer > :nth-child(1) > .fa-solid").click();
    // Item should exist
    cy.get(":nth-child(1) > result-item-ui > .item").should("exist");
    // Item data should match the user's input and result detail info
    cy.get(":nth-child(1) > result-item-ui > .item > .item__title").contains(
      "Bilbao, Spain"
    );
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__list > :nth-child(1) > span"
    ).contains("8:02 AM");
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__list > :nth-child(2) > span"
    ).contains("8:02 PM");
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__list > :nth-child(3) > span"
    ).contains("2:02 PM");
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__list > :nth-child(4) > span"
    ).contains("11:59");
    cy.get(
      ":nth-child(1) > result-item-ui > .item > .item__list > :nth-child(5) > span"
    ).contains("1990-09-27");
    // Default result interface
    cy.get("result-logo-ui").should("exist");
  });

  it("user can remove the result from the screen by clicking on delete button", () => {
    // Click delete
    cy.get(".result__footer > :nth-child(2) > .fa-solid").click();
    // Item should not exist
    cy.get(":nth-child(1) > result-item-ui > .item").should("not.exist");
    // Default result interface
    cy.get("result-logo-ui").should("exist");
  });
});
